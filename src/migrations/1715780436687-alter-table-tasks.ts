import { MigrationInterface, QueryRunner } from "typeorm";

export class AlterTableTasks1715780436687 implements MigrationInterface {
    name = 'AlterTableTasks1715780436687'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            ALTER TABLE "task"
            ADD "collaborators" text
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            ALTER TABLE "task" DROP COLUMN "collaborators"
        `);
    }

}
