import { MigrationInterface, QueryRunner } from 'typeorm';

export class AlterUserTableDropColumnsRole1715764484448
  implements MigrationInterface
{
  name = 'AlterUserTableDropColumnsRole1715764484448';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
            ALTER TABLE "user" DROP COLUMN "role"
        `);
    await queryRunner.query(`
            DROP TYPE "public"."user_role_enum"
        `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
            CREATE TYPE "public"."user_role_enum" AS ENUM('superadmin', 'standard')
        `);
    await queryRunner.query(`
            ALTER TABLE "user"
            ADD "role" "public"."user_role_enum" DEFAULT 'superadmin'
        `);
  }
}
