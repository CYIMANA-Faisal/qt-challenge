import { MigrationInterface, QueryRunner } from "typeorm";

export class CreateTableTasks1715767040482 implements MigrationInterface {
    name = 'CreateTableTasks1715767040482'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            CREATE TYPE "public"."task_priority_enum" AS ENUM('low', 'normal', 'high')
        `);
        await queryRunner.query(`
            CREATE TYPE "public"."task_savedas_enum" AS ENUM('draft', 'final')
        `);
        await queryRunner.query(`
            CREATE TABLE "task" (
                "id" SERIAL NOT NULL,
                "createdAt" TIMESTAMP NOT NULL DEFAULT now(),
                "updatedAt" TIMESTAMP NOT NULL DEFAULT now(),
                "deletedAt" TIMESTAMP,
                "title" character varying NOT NULL,
                "startDate" date NOT NULL DEFAULT ('now'::text)::date,
                "endDate" date NOT NULL DEFAULT ('now'::text)::date,
                "assignees" text NOT NULL,
                "project" character varying NOT NULL,
                "description" character varying NOT NULL,
                "priority" "public"."task_priority_enum" NOT NULL DEFAULT 'normal',
                "attachment" character varying,
                "savedAs" "public"."task_savedas_enum" NOT NULL DEFAULT 'draft',
                CONSTRAINT "PK_fb213f79ee45060ba925ecd576e" PRIMARY KEY ("id")
            )
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            DROP TABLE "task"
        `);
        await queryRunner.query(`
            DROP TYPE "public"."task_savedas_enum"
        `);
        await queryRunner.query(`
            DROP TYPE "public"."task_priority_enum"
        `);
    }

}
