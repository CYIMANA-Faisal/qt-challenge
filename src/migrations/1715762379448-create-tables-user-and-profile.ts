import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateTablesUserAndProfile1715762379448
  implements MigrationInterface
{
  name = 'CreateTablesUserAndProfile1715762379448';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
            CREATE TYPE "public"."profile_gender_enum" AS ENUM('male', 'female', 'other', 'preferNotToSay')
        `);
    await queryRunner.query(`
            CREATE TYPE "public"."profile_levelofeducation_enum" AS ENUM(
                'highschool',
                'undergraduate',
                'graduate',
                'postgraduate'
            )
        `);
    await queryRunner.query(`
            CREATE TABLE "profile" (
                "id" SERIAL NOT NULL,
                "createdAt" TIMESTAMP NOT NULL DEFAULT now(),
                "updatedAt" TIMESTAMP NOT NULL DEFAULT now(),
                "deletedAt" TIMESTAMP,
                "displayName" character varying,
                "bio" character varying,
                "proffesion" character varying,
                "location" character varying,
                "gender" "public"."profile_gender_enum" DEFAULT 'preferNotToSay',
                "levelOfEducation" "public"."profile_levelofeducation_enum" DEFAULT 'graduate',
                CONSTRAINT "PK_3dd8bfc97e4a77c70971591bdcb" PRIMARY KEY ("id")
            )
        `);
    await queryRunner.query(`
            CREATE TYPE "public"."user_role_enum" AS ENUM('superadmin', 'standard')
        `);
    await queryRunner.query(`
            CREATE TABLE "user" (
                "id" SERIAL NOT NULL,
                "createdAt" TIMESTAMP NOT NULL DEFAULT now(),
                "updatedAt" TIMESTAMP NOT NULL DEFAULT now(),
                "deletedAt" TIMESTAMP,
                "name" character varying NOT NULL,
                "email" character varying NOT NULL,
                "password" character varying NOT NULL,
                "role" "public"."user_role_enum" DEFAULT 'superadmin',
                "profileId" integer,
                CONSTRAINT "UQ_e12875dfb3b1d92d7d7c5377e22" UNIQUE ("email"),
                CONSTRAINT "REL_9466682df91534dd95e4dbaa61" UNIQUE ("profileId"),
                CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY ("id")
            )
        `);
    await queryRunner.query(`
            ALTER TABLE "user"
            ADD CONSTRAINT "FK_9466682df91534dd95e4dbaa616" FOREIGN KEY ("profileId") REFERENCES "profile"("id") ON DELETE NO ACTION ON UPDATE NO ACTION
        `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
            ALTER TABLE "user" DROP CONSTRAINT "FK_9466682df91534dd95e4dbaa616"
        `);
    await queryRunner.query(`
            DROP TABLE "user"
        `);
    await queryRunner.query(`
            DROP TYPE "public"."user_role_enum"
        `);
    await queryRunner.query(`
            DROP TABLE "profile"
        `);
    await queryRunner.query(`
            DROP TYPE "public"."profile_levelofeducation_enum"
        `);
    await queryRunner.query(`
            DROP TYPE "public"."profile_gender_enum"
        `);
  }
}
