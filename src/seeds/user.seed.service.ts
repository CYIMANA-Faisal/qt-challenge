import { Injectable } from '@nestjs/common';
import { EntityManager } from 'typeorm';
import { BcryptService } from '../common/services/bcrypt.service';
import { ConfigService } from '@nestjs/config';
import { User } from '../users/entities/user.entity';

/**
 * Service responsible for seeding the database with a starter user.
 * @remarks
 * This service checks for the existence of a starter user and creates one if it doesn't exist.
 * @public
 */
@Injectable()
export class UserSeedService {
  constructor(
    private readonly entityManager: EntityManager,
    private readonly bcryptService: BcryptService,
    public readonly configService: ConfigService,
  ) {}

  /**
   * Creates a starter user if one does not already exist.
   * @returns A promise that resolves when the starter user is created or already exists.
   */
  async createStarterUser(): Promise<void> {
    // Retrieve starter user email from configuration
    const starterUserEmail =
      this.configService.get<string>('STARTER_USER_EMAIL');

    // Check if a user with the starter email already exists
    const existingUser = await this.entityManager.findOne(User, {
      where: { email: starterUserEmail },
    });

    // If the user exists, exit the function
    if (existingUser) {
      return;
    }

    // Create a new user entity
    const user = new User();
    user.email = starterUserEmail;
    // Hash the starter user password retrieved from configuration
    user.password = await this.bcryptService.hash(
      this.configService.get<string>('STARTER_USER_PASSWORD'),
    );
    user.name = 'Starter User';

    // Save the new user to the database
    await this.entityManager.save(User, user);
  }
}
