import { Column, Entity, JoinColumn, OneToOne, Unique } from 'typeorm';
import { Audit } from '../../common/utils/audit.entity';
import { Exclude } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { Profile } from '../../profiles/entities/profile.entity';

@Entity()
@Unique(['email'])
export class User extends Audit {
  @ApiProperty()
  @Column({ nullable: false })
  name: string;

  @ApiProperty()
  @Column({ nullable: false })
  email: string;

  @Column({ nullable: false })
  @Exclude()
  password: string;

  @OneToOne(() => Profile)
  @JoinColumn()
  profile: Profile;
}
