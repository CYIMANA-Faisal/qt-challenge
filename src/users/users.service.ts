import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './entities/user.entity';
import { CreateUserDTO } from './dto/create-user-dto';

/**
 * Service responsible for user management operations.
 * @remarks
 * This service provides functionality for creating and finding users.
 * @public
 */
@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {}

  /**
   * Creates a new user.
   * @param user - User to create.
   * @returns A promise resolving to the user created.
   */

  async createUser(createUserDto: CreateUserDTO): Promise<User> {
    const user = this.usersRepository.create(createUserDto);
    return await this.usersRepository.save(user);
  }

  /**
   * Finds a user by email.
   * @param email - Email of the user to find.
   * @returns A promise resolving to the user found by the email.
   */
  async findUserByEmail(email: string): Promise<User> {
    return await this.usersRepository.findOne({ where: { email: email } });
  }

  async changePassword(id: number, newPassword: string): Promise<void> {
    await this.usersRepository.update({ id: id }, { password: newPassword });
  }
}
