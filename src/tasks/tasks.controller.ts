import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  HttpCode,
  HttpStatus,
} from '@nestjs/common';
import { TasksService } from './tasks.service';
import { CreateTaskDto } from './dto/create-task.dto';
import { UpdateTaskDto } from './dto/update-task.dto';
import { ApiResponse } from '../common/interfaces/api-response.interface';
import { Task } from './entities/task.entity';

@Controller('tasks')
export class TasksController {
  constructor(private readonly tasksService: TasksService) {}

  @Post()
  @HttpCode(HttpStatus.CREATED)
  async create(
    @Body() createTaskDto: CreateTaskDto,
  ): Promise<ApiResponse<string>> {
    await this.tasksService.create(createTaskDto);
    return { message: 'Task created successfully', payload: null };
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  async findAll(): Promise<ApiResponse<Task[]>> {
    const results = await this.tasksService.findAll();
    return { message: 'Task retrieved successfully', payload: results };
  }

  @Get(':id')
  @HttpCode(HttpStatus.OK)
  async findOne(@Param('id') id: string): Promise<ApiResponse<Task>> {
    const results = await this.tasksService.findOne(+id);
    return { message: 'task retireved successfully', payload: results };
  }

  @Patch(':id')
  @HttpCode(HttpStatus.OK)
  async update(
    @Param('id') id: string,
    @Body() updateTaskDto: UpdateTaskDto,
  ): Promise<ApiResponse<Task>> {
    const results = await this.tasksService.update(+id, updateTaskDto);
    return {
      message: 'Task updated successfully',
      payload: results,
    };
  }

  @HttpCode(HttpStatus.OK)
  @Delete(':id')
  async remove(@Param('id') id: string): Promise<ApiResponse<string>> {
    await this.tasksService.remove(+id);
    return { message: 'Task deleted successfully', payload: null };
  }
}
