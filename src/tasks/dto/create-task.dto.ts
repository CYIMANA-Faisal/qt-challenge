import {
  IsArray,
  IsDateString,
  IsEnum,
  IsNotEmpty,
  IsOptional,
  IsString,
  IsUrl,
  Length,
} from 'class-validator';
import { EPriority } from '../../common/enums/priority.enum';
import { ESavedAs } from '../../common/enums/saved-as.enum';

/**
 * Data Transfer Object for creating a new Task.
 */
export class CreateTaskDto {
  /**
   * Title of the task.
   * @example "Implement authentication"
   */
  @IsString()
  @IsNotEmpty()
  title: string;

  /**
   * Start date of the task.
   * Must be a valid ISO 8601 date string.
   * @example "2024-05-15"
   */
  @IsDateString()
  @IsNotEmpty()
  startDate: Date;

  /**
   * End date of the task.
   * Must be a valid ISO 8601 date string.
   * @example "2024-05-20"
   */
  @IsDateString()
  @IsNotEmpty()
  endDate: Date;

  /**
   * List of assignee IDs.
   * @example ["jojo", "jane", "jake]
   */
  @IsArray()
  @IsNotEmpty({ each: true })
  assignees: string[];

  /**
   * List of assignee IDs.
   * @example ["jojo", "jane", "jake]
   */
  @IsArray()
  @IsNotEmpty({ each: true })
  collaborators: string[];

  /**
   * Project name to which the task belongs.
   * @example "Project A"
   */
  @IsString()
  @IsNotEmpty()
  project: string;

  /**
   * Detailed description of the task.
   * @example "This task involves setting up the authentication module using NestJS."
   */
  @IsString()
  @IsNotEmpty()
  @Length(1, 30)
  description: string;

  /**
   * Priority of the task.
   * Must be one of the values defined in the EPriority enum.
   * @example EPriority.high
   */
  @IsEnum(EPriority)
  @IsNotEmpty()
  priority: EPriority;

  /**
   * Attachment URL or path.
   * @example "http://example.com/attachment.pdf"
   */
  @IsOptional()
  @IsUrl()
  attachment?: string;

  /**
   * Status indicating whether the task is saved as draft or final.
   * Must be one of the values defined in the ESavedAs enum.
   * @example ESavedAs.final
   */
  @IsEnum(ESavedAs)
  @IsNotEmpty()
  savedAs: ESavedAs;
}
