import { Column, Entity } from 'typeorm';
import { Audit } from '../../common/utils/audit.entity';
import { EPriority } from '../../common/enums/priority.enum';
import { ESavedAs } from '../../common/enums/saved-as.enum';

/**
 * Represents a task in the system.
 * Inherits common audit properties such as created and updated timestamps from Audit.
 */
@Entity()
export class Task extends Audit {
  /**
   * Title of the task.
   */
  @Column({ nullable: false })
  title: string;

  /**
   * Start date of the task.
   */
  @Column({ type: 'date', nullable: false, default: () => 'CURRENT_DATE' })
  startDate: Date;

  /**
   * End date of the task.
   */
  @Column({ type: 'date', nullable: false, default: () => 'CURRENT_DATE' })
  endDate: Date;

  /**
   * List of assignees.
   */
  @Column('simple-array', { nullable: false })
  assignees: string[];

  /**
   * List of collaborators.
   */
  @Column('simple-array', { nullable: true })
  collaborators: string[];

  /**
   * Project name or ID to which the task belongs.
   * Consider using a relation if `project` refers to another entity.
   */
  @Column({ nullable: false })
  project: string;

  /**
   * Detailed description of the task.
   */
  @Column({ nullable: false })
  description: string;

  /**
   * Priority of the task.
   */
  @Column({
    type: 'enum',
    enum: EPriority,
    nullable: false,
    default: EPriority.normal,
  })
  priority: EPriority;

  /**
   * Attachment URL or path.
   */
  @Column({ nullable: true })
  attachment: string;

  /**
   * Status indicating whether the task is saved as draft or final.
   */
  @Column({
    type: 'enum',
    enum: ESavedAs,
    nullable: false,
    default: ESavedAs.draft,
  })
  savedAs: ESavedAs;
}
