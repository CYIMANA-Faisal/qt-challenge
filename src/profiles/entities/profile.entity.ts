import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity } from 'typeorm';
import { ELevelOfEducation } from '../../common/enums/level-of-education.enum';
import { Audit } from '../../common/utils/audit.entity';
import { EGender } from '../../common/enums/gender.enum';

@Entity()
export class Profile extends Audit {
  @ApiProperty()
  @Column({ nullable: true })
  displayName: string;

  @ApiProperty()
  @Column({ nullable: true })
  bio: string;

  @ApiProperty()
  @Column({ nullable: true })
  proffesion: string;

  @ApiProperty()
  @Column({ nullable: true })
  location: string;

  @ApiProperty()
  @Column({
    type: 'enum',
    enum: EGender,
    default: EGender.preferNotToSay,
    nullable: true,
  })
  gender: string;

  @ApiProperty()
  @Column({
    type: 'enum',
    enum: ELevelOfEducation,
    default: ELevelOfEducation.graduate,
    nullable: true,
  })
  levelOfEducation: ELevelOfEducation;
}
