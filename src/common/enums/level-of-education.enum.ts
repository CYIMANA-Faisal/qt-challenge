export enum ELevelOfEducation {
  highschool = 'highschool',
  undergraduate = 'undergraduate',
  graduate = 'graduate',
  postgraduate = 'postgraduate',
}
