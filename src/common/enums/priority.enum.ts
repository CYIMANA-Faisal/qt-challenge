export enum EPriority {
  low = 'low',
  normal = 'normal',
  high = 'high',
}
