export enum EGender {
  male = 'male',
  female = 'female',
  other = 'other',
  preferNotToSay = 'preferNotToSay',
}
