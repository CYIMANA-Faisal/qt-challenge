export enum ESavedAs {
  draft = 'draft',
  final = 'final',
}
