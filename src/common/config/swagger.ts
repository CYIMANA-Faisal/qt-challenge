import { DocumentBuilder } from '@nestjs/swagger';

const swaggerOptions = {
  title: 'MisiInfo API',
  description: 'Swagger documentation for MisiInfo API',
  version: '1.0.0',
  docExpansion: 'none',
};

export const options = new DocumentBuilder()
  .setTitle(swaggerOptions.title)
  .setDescription(swaggerOptions.description)
  .setVersion(swaggerOptions.version)
  .addBearerAuth()
  .build();

export const url = 'swagger-docs';
