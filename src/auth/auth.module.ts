import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { UsersModule } from '../users/users.module';
import { UsersService } from '../users/users.service';
import { BcryptService } from '../common/services/bcrypt.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../users/entities/user.entity';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { JwtStrategy } from './strategy/jwt.strategy';

/**
 * The AuthModule is responsible for handling the authentication logic
 * and integrates various services, controllers, and modules required for authentication.
 *
 * @remarks
 * This module imports necessary modules, sets up providers, and configures JWT and Passport for authentication.
 */
@Module({
  imports: [
    UsersModule,
    // Import TypeOrmModule for the User entity
    TypeOrmModule.forFeature([User]),
    // Configure Passport module with JWT strategy
    PassportModule.register({
      defaultStrategy: 'jwt',
      property: 'user',
      session: false,
    }),
    // Configure JwtModule asynchronously using ConfigService
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        privateKey: configService.get<string>('keys.privateKey'),
        publicKey: configService.get<string>('keys.publicKey'),
      }),
      inject: [ConfigService],
    }),
  ],
  controllers: [AuthController],
  providers: [
    AuthService, // Service handling authentication logic
    UsersService, // Service managing user-related operations
    BcryptService, // Service for handling password hashing
    JwtStrategy, // JWT strategy for Passport authentication
  ],
  exports: [
    AuthService, // Export AuthService for use in other modules
    JwtModule, // Export JwtModule for JWT-related functionality
    PassportModule, // Export PassportModule for authentication
  ],
})
export class AuthModule {}
