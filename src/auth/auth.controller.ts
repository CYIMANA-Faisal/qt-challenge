import {
  Body,
  Controller,
  HttpCode,
  HttpStatus,
  Patch,
  Post,
  UseGuards,
} from '@nestjs/common';
import {
  ApiOperation,
  ApiTags,
  ApiBadRequestResponse,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { AuthService } from './auth.service';
import { LoginDto } from './dto/login.dto';
import { Token } from './interfaces/token.interface';
import { ApiResponse } from '../common/interfaces/api-response.interface';
import { CreateUserDTO } from '../users/dto/create-user-dto';
import { ChangePasswordDto } from './dto/change-password.dto';
import { GetUser } from '../common/decorators/user.decorator';
import { AuthGuard } from '@nestjs/passport';
import { User } from '../users/entities/user.entity';

/**
 * AuthController handles authentication-related HTTP requests.
 *
 * @remarks
 * This controller provides endpoints for user registration and login.
 *
 * @public
 */
@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  /**
   * Registers a new user.
   *
   * @param createUserDto - Data Transfer Object containing user registration information.
   * @returns A promise resolving to an API response indicating successful registration.
   *
   * @example
   * POST /auth/register
   * {
   *   "email": "example@example.com",
   *   "password": "password123",
   *   "name": "John Doe"
   * }
   *
   * @response 201 - User registered successfully
   * @response 400 - Bad Request, invalid data
   */
  @Post('register')
  async register(
    @Body() createUserDto: CreateUserDTO,
  ): Promise<ApiResponse<string>> {
    await this.authService.register(createUserDto);
    return { message: 'User registered successfully', payload: null };
  }

  /**
   * Endpoint for user login.
   *
   * @param loginDto - Data Transfer Object containing login credentials.
   * @returns A promise resolving to an API response containing an authentication token.
   *
   * @example
   * POST /auth/login
   * {
   *   "email": "example@example.com",
   *   "password": "password123"
   * }
   *
   * @response 201 - Login successful, returns the authentication token
   * @response 400 - Bad Request, invalid credentials or missing data
   * @response 401 - Unauthorized, invalid credentials
   */
  @Post('login')
  @HttpCode(HttpStatus.CREATED)
  @ApiOperation({ summary: 'User Login' })
  @ApiBadRequestResponse({
    description: 'Invalid credentials or missing data',
  })
  @ApiUnauthorizedResponse({
    description: 'Unauthorized access',
  })
  async login(@Body() loginDto: LoginDto): Promise<ApiResponse<Token>> {
    const payload = await this.authService.login(loginDto);
    return { message: 'Login successfully', payload: payload };
  }

  @Patch('change-password')
  @UseGuards(AuthGuard('jwt'))
  async changePassword(
    @GetUser() user: User,
    @Body()
    changePasswordDto: ChangePasswordDto,
  ): Promise<ApiResponse<string>> {
    await this.authService.changePassword(user, changePasswordDto);
    return { message: 'Password changed Successfully', payload: null };
  }
}
