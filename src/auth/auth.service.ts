import {
  BadRequestException,
  ConflictException,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { BcryptService } from '../common/services/bcrypt.service';
import { TokenPayload } from './interfaces/jwt.payload.interface';
import { JwtService } from '@nestjs/jwt';
import { LoginDto } from './dto/login.dto';
import { Token } from './interfaces/token.interface';
import { ConfigService } from '@nestjs/config';
import { CreateUserDTO } from '../users/dto/create-user-dto';
import { ChangePasswordDto } from './dto/change-password.dto';
import { User } from '../users/entities/user.entity';

/**
 * Service responsible for authentication-related operations.
 * @remarks
 * This service handles user registration, login, and token generation.
 * @public
 */
@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UsersService,
    private readonly bcryptService: BcryptService,
    private readonly jwtService: JwtService,
    private readonly configService: ConfigService,
  ) {}

  /**
   * Registers a new user.
   * @param createUserDto - Data Transfer Object containing user registration information.
   * @returns A promise that resolves when the user is successfully registered.
   * @throws ConflictException - If a user with the provided email already exists.
   */
  async register(createUserDto: CreateUserDTO): Promise<void> {
    const userExist = await this.userService.findUserByEmail(
      createUserDto.email,
    );

    if (userExist) {
      throw new ConflictException(
        'User already exists, please use a different email',
      );
    }

    const hashedPassword = await this.bcryptService.hash(
      createUserDto.password,
    );
    createUserDto.password = hashedPassword;

    await this.userService.createUser(createUserDto);
  }

  /**
   * Logs in a user.
   * @param loginDto - Data Transfer Object containing login credentials.
   * @returns A promise resolving to a token for authentication.
   * @throws UnauthorizedException - If credentials are invalid.
   */
  async login(loginDto: LoginDto): Promise<Token> {
    const user = await this.userService.findUserByEmail(loginDto.email);
    if (!user) {
      throw new UnauthorizedException('Invalid credentials, please try again');
    }

    const isValidPassword = await this.bcryptService.compare(
      loginDto.password,
      user.password,
    );
    if (!isValidPassword) {
      throw new UnauthorizedException('Invalid credentials, please try again');
    }

    const tokens = {
      accessToken: await this.generateJwtAccessToken(user.id, user.email),
    };
    return tokens;
  }

  /**
   * Generates a JWT access token for a user.
   * @param userId - ID of the user.
   * @param email - Email of the user.
   * @returns A promise resolving to the JWT access token.
   */
  async generateJwtAccessToken(userId: number, email: string): Promise<string> {
    const payload: TokenPayload = { id: userId, email: email };
    const token = await this.jwtService.signAsync(payload, {
      expiresIn: this.configService.get<string>(
        'JWT_ACCESS_TOKEN_EXPIRATION_TIME',
      ),
      algorithm: 'RS256',
    });
    return token;
  }

  async changePassword(
    user: User,
    changePasswordDto: ChangePasswordDto,
  ): Promise<void> {
    const isValidOldPassword = await this.bcryptService.compare(
      changePasswordDto.oldPassword,
      user.password,
    );
    if (!isValidOldPassword) {
      throw new BadRequestException('Incorect old password');
    }
    const NewHashedPassword = await this.bcryptService.hash(
      changePasswordDto.newPassword,
    );
    await this.userService.changePassword(user.id, NewHashedPassword);
  }
}
